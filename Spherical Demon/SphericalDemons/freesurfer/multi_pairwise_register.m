function multi_pairwise_register(fixed_surface, moving_surface, feature_name)

subject_num = length(fixed_surface);

for i = 1:subject_num
    pairwise_register(fixed_surface{i},moving_surface,feature_name);
end

end

