function indices = square_dist(source,target,len_limit)

% source (3,Ns)
% target (3,Nt)
% indices (1,Nt)

Ns = size(source,2);
Nt = size(target,2);

s = 1;
e = len_limit;
iter = ceil(Nt/len_limit);
indices = zeros(1,Nt);
for i = 1:iter
    len = len_limit;
    if i == iter
        len = Nt - len_limit*(iter-1);
        e = Nt;
    end
    dist = zeros(len,Ns);
    temp1 = reshape(sum(target(:,s:e).^2,1),[len,1]);
    temp2 = reshape(sum(source.^2,1),[1,Ns]);
    
    dist = repmat(temp1,1,Ns);
    dist = dist + repmat(temp2,len,1);
    dist = dist - 2*(target(:,s:e).'*source);
    
    [value,index] = min(dist,[],2);
    indices(s:e) = index;
    
    s = s + len_limit;
    e = e + len_limit;
end

end

