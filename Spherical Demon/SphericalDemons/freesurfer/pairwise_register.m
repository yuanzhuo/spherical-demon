function pairwise_register( fixed_surface, moving_surface, feature_name, disp_iter )

if(nargin < 3)
   disp(' ');
   disp('mris_SD_pairwise_register(fixed_surface, moving_surface, output_surface, <optional> disp_iter)');
   return;
end

[uni_vertices,uni_faces] = read_surf(moving_surface);

%%%%%%%%%%%%%%%%%%%%%%%%
% Read fixed surface
%%%%%%%%%%%%%%%%%%%%%%%%

% Grab hemi, SUBJECTS_DIR from fixed_surface.
indices = strfind(fixed_surface, filesep);
hemi = fixed_surface(indices(end)+1:indices(end)+2);
SUBJECTS_DIR = fixed_surface(1:indices(end-2)-1);
subject = fixed_surface(indices(end-2)+1:indices(end-1)-1);
surf_dir = fixed_surface(indices(end-1)+1:indices(end)-1); 
if(strcmp(surf_dir, 'surf') == 0)
   error(['mris_SD_register assumes surfaces and data are in the "surf" directory of the subject, not ' surf_dir]); 
end

SD_atlas = CreateDefaultFreeSurferAtlas(hemi);
SD_atlas.parms.uniform_mesh_dir = fullfile('..','..');
SD_atlas.parms.surf_filename = fixed_surface(indices(end)+4:end); 
SD_atlas.parms.SUBJECT = subject;
SD_atlas.parms.SUBJECTS_DIR = SUBJECTS_DIR;

SD_atlas.parms

annot_path = fullfile(SUBJECTS_DIR,subject,'label',[hemi '.aparc.annot']);
save_path = fullfile(SUBJECTS_DIR,subject,'mat',[hemi '_reg' '.mat']);

% Read fixed subject mesh
disp(['Read fixed subject ' num2str(subject) ': ' SD_atlas.parms.hemi '.' SD_atlas.parms.surf_filename]);
disp(['and data: ' SD_atlas.parms.data_filename_cell]);

if(~isfield(SD_atlas.parms, 'read_surface'))
    fixed_sbjMesh = MARS2_readSbjMesh(SD_atlas.parms);
else
    fixed_sbjMesh = feval(SD_atlas.parms.read_surface, SD_atlas.parms);
end

num_data = length(feature_name);
data = zeros(num_data,length(fixed_sbjMesh.vertices));
for i = 1:num_data
    [feat,fnum] = read_curv([SD_atlas.parms.SUBJECTS_DIR '/' SD_atlas.parms.SUBJECT '/surf/' SD_atlas.parms.hemi '.' feature_name{i}]);
    data(i,:) = feat;
end

%%%%%%%%%%%%%%%%%%%%%%%%
% Read moving surface
%%%%%%%%%%%%%%%%%%%%%%%%

% Grab hemi, SUBJECTS_DIR from moving_surface.
indices = strfind(moving_surface, filesep);
hemi = moving_surface(indices(end)+1:indices(end)+2);
SUBJECTS_DIR = moving_surface(1:indices(end-2)-1);
subject = moving_surface(indices(end-2)+1:indices(end-1)-1);
surf_dir = moving_surface(indices(end-1)+1:indices(end)-1); 
if(strcmp(surf_dir, 'surf') == 0)
   error(['mris_SD_register assumes surfaces and data are in the "surf" directory of the subject, not ' surf_dir]); 
end

% check hemi
if(strcmp(SD_atlas.parms.hemi, hemi) == 0)
   error(['Fixed surface uses hemisphere ' SD_atlas.parms.hemi ', but moving_surface is ' hemi]);
end

SD_atlas.parms.surf_filename = moving_surface(indices(end)+4:end); 
SD_atlas.parms.SUBJECT = subject;
SD_atlas.parms.SUBJECTS_DIR = SUBJECTS_DIR;

SD_atlas.parms

% Read fixed subject mesh
disp(['Read moving subject ' num2str(subject) ': ' SD_atlas.parms.hemi '.' SD_atlas.parms.surf_filename]);
disp(['and data: ' SD_atlas.parms.data_filename_cell]);

if(~isfield(SD_atlas.parms, 'read_surface'))
    moving_sbjMesh = MARS2_readSbjMesh(SD_atlas.parms);
else
    moving_sbjMesh = feval(SD_atlas.parms.read_surface, SD_atlas.parms);
end


% initialize reg_parms (These are the defaults)
reg_parms = CreateDefaultFreeSurferRegParms; 
reg_parms.iter = [15 15 15 15];                    

reg_parms.smooth_displacement_iter = [10 10 10 10]; 

if(nargin == 4)                                                    
    reg_parms.smooth_displacement_iter(:) = str2num(disp_iter);                                                    
end
    
reg_parms.smooth_velocity = 0;                      
reg_parms.smooth_velocity_iter = [4 4 4 4];                                           
reg_parms.final_unfold = 0;

% =========================================================================
% Real code begins!                                                    
% =========================================================================                                                                                                        

% Read in uniform meshes.
disp('Read Uniform Meshes');
reg_parms.meshes{1} = MARS_readUniformMesh(SD_atlas.parms.uniform_mesh_dir, 'ic4.tri');
reg_parms.meshes{2} = MARS_readUniformMesh(SD_atlas.parms.uniform_mesh_dir, 'ic5.tri');
reg_parms.meshes{3} = MARS_readUniformMesh(SD_atlas.parms.uniform_mesh_dir, 'ic6.tri');
reg_parms.meshes{4} = MARS_readUniformMesh(SD_atlas.parms.uniform_mesh_dir, 'ic7.tri');


% Register subject
disp(['Register moving surface and fixed surface']);
tic
reg_parms.sbjWarp = []; %no initialization
reg_parms = SD_registerPairOfSpheres(fixed_sbjMesh, moving_sbjMesh, SD_atlas, reg_parms);
sbjWarp = reg_parms.sbjWarp;
toc

sbjWarp.vertices = sbjWarp.curr_vertices;
sbjWarp.faces = fixed_sbjMesh.faces;
sbjWarp.vertexNbors = fixed_sbjMesh.vertexNbors;
sbjWarp.vertexFaces = fixed_sbjMesh.vertexFaces;

disp('Ѱ�������');
tic;
Indices = square_dist(sbjWarp.curr_vertices,uni_vertices',4000);
toc;
[v, label, c] = read_annotation(annot_path);

final_label = label(Indices);
final_data = MARS_linearInterpolate(uni_vertices', sbjWarp, data);  % ��ֵ

temp_surface = zeros(size(uni_faces,1),4);
temp_surface(:,1) = 3*ones(size(uni_faces,1),1);
temp_surface(:,2:end) = int32(uni_faces);

vtk = struct('vertices',uni_vertices,'faces',temp_surface,'par_fs',final_label','data',final_data);
vtk.feature_name = feature_name;

disp(['Saving final warp vtk in ' save_path]);
save(save_path,'vtk');

end

